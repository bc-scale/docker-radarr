# docker-radarr

[![pipeline status](https://gitlab.com/homesrvr/docker-radarr/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-radarr/commits/main) 
[![radarr Release](https://gitlab.com/homesrvr/docker-radarr/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-radarr/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-radarr/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/radarr)

alpine-based dockerized build of radarr.
This is part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server. It is a lightweight image.

# Docker
The resulting docker image can be found here [https://hub.docker.com/r/culater/radarr](https://hub.docker.com/r/culater/radarr)

## Example usage

docker cli run example:
````
docker run -d \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/Berlin \
  -p 7878:7878 \
  -v /path/to/config:/config \
  -v /path/to/movies:/movies `#optional but highly recommended` \
  -v /path/to/downloads:/downloads `#optional` \
  --restart unless-stopped \
  culater/radarr
````

docker-compose example:
````
---
version: '3'  
  radarr:
    image: culater/radarr:latest
    container_name: radarr
    hostname: radarr
    environment:
      - PUID=1001
      - PGID=1000
      - TZ=Europe/Berlin
    volumes:
      - /path/to/config:/config
      - /media/movies:/movies 
      - /downloads:/downloads 
    ports:
      - 7878:7878
    restart: always
````
## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-radarr/-/issues)

## Authors and acknowledgment
More information about radarr can be found here:
[radarr](https://radarr.video "Radarr Project Homepage") 


## Project status
The docker image auto-updates after a new release of radarr within a few days. 
